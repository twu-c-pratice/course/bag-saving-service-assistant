Story 6: 蠢萌存包小弟

AC 1 : save bag

Given a stupid assistant and a not full cabinet, when given him a small bag, then should get a ticket.
Given a stupid assistant and a not full cabinet, when given him small medium big bags, then should get a ticket.
Given a stupid assistant and a not full cabinet, when given him mini and huge bags, then should get a error.
Given a stupid assistant and a not full cabinet, when given him null to save, then should get a error.
Given a stupid assistant and a cabinet which small locker size is full, when given given small bag to save, it should throw an error.

AC 2: get bag

Given a stupid assistant, when given him a ticket, then should get the bag.
Given a stupid assistant, when given him null to get bag, then should throw a error.

Story 7:

AC 1: Cabinet Cluster

Given a Cabinet Cluster with one cabinet, when the assistant save the bag, it should get the ticket and get the bag by the ticket.
Given a Cabinet Cluster with one cabinet, when the assistant save the mini and huge bag, it should get a exception.
Given a Cabinet Cluster with two cabinet but the first is full, when the assistant save the bag, it should get the ticket and get the bag by the ticket.

AC2 : Cabinet Cluster With Assistants

Given a Cabinet Cluster with one cabinet and two assistant, when the first assistant save the bag, the second assistant should get the bag by the ticket.