package com.twuc.bagSaving;

public class StupidAssistant {
    private Cabinet cabinet;

    public StupidAssistant(Cabinet cabinet) {
        this.cabinet = cabinet;
    }

    private static LockerSize getLockerSizeFromBagSize(BagSize bagSize) {
        return LockerSize.valueOf(bagSize.toString());
    }

    public Ticket save(Bag bag) {
        if (bag == null) {
            throw new IllegalArgumentException();
        }

        LockerSize lockerSize;
        try {
            lockerSize = getLockerSizeFromBagSize(bag.getBagSize());
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }

        Ticket ticket;

        ticket = cabinet.save(bag, lockerSize);

        return ticket;
    }

    public Bag get(Ticket ticket) {
        Bag bag;

        bag = cabinet.getBag(ticket);

        return bag;
    }
}
