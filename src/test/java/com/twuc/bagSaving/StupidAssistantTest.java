package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class StupidAssistantTest {

    private StupidAssistant assistant;

    void setAssistantWithPlentyOfCapacityCabinet() {
        Cabinet cabinet = CabinetFactory.createCabinetWithPlentyOfCapacity();
        assistant = new StupidAssistant(cabinet);
    }

    private BagSize getBagSizeFromLockerSize(LockerSize lockerSize) {
        return BagSize.valueOf(lockerSize.toString());
    }

    @ParameterizedTest
    @EnumSource(
            value = BagSize.class,
            names = {"SMALL", "MEDIUM", "BIG"},
            mode = EnumSource.Mode.INCLUDE)
    void should_save_bag_with_stupid_assistant(BagSize bagSize) {
        setAssistantWithPlentyOfCapacityCabinet();
        Bag bag = new Bag(bagSize);
        Ticket ticket = assistant.save(bag);

        assertNotNull(ticket);
    }

    @ParameterizedTest
    @EnumSource(
            value = BagSize.class,
            names = {"MINI", "HUGE"},
            mode = EnumSource.Mode.INCLUDE)
    void should_get_exception_when_without_exact_locker_size(BagSize bagSize) {
        setAssistantWithPlentyOfCapacityCabinet();
        Bag bag = new Bag(bagSize);

        assertThrows(IllegalArgumentException.class, () -> assistant.save(bag));
    }

    @Test
    void should_get_exception_when_given_null_to_save() {
        setAssistantWithPlentyOfCapacityCabinet();

        assertThrows(IllegalArgumentException.class, () -> assistant.save(null));
    }

    @ParameterizedTest
    @EnumSource(LockerSize.class)
    void should_get_exception_when_the_locker_is_full(LockerSize lockerSize) {
        BagSize bagSize = getBagSizeFromLockerSize(lockerSize);
        Cabinet cabinet = new Cabinet(LockerSetting.of(lockerSize, 1));
        cabinet.save(new Bag(bagSize), lockerSize);

        StupidAssistant assistant = new StupidAssistant(cabinet);
        Bag bag = new Bag(bagSize);

        assertThrows(InsufficientLockersException.class, () -> assistant.save(bag));
    }

    @ParameterizedTest
    @EnumSource(
            value = BagSize.class,
            names = {"SMALL", "MEDIUM", "BIG"},
            mode = EnumSource.Mode.INCLUDE)
    void should_get_the_bag_with_ticket(BagSize bagSize) {
        setAssistantWithPlentyOfCapacityCabinet();
        Bag exceptedBag = new Bag(bagSize);
        Ticket ticket = assistant.save(exceptedBag);

        Bag bag = assistant.get(ticket);

        assertSame(exceptedBag, bag);
    }

    @ParameterizedTest
    @EnumSource(
            value = BagSize.class,
            names = {"SMALL", "MEDIUM", "BIG"},
            mode = EnumSource.Mode.INCLUDE)
    void should_get_exception_when_the_ticket_is_null() {
        setAssistantWithPlentyOfCapacityCabinet();

        assertThrows(IllegalArgumentException.class, () -> assistant.get(null));
    }
}
