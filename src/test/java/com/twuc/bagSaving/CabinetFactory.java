package com.twuc.bagSaving;

import java.util.Arrays;

class CabinetFactory {
    static Cabinet createCabinetWithPlentyOfCapacity() {
        LockerSetting[] settings =
            Arrays.stream(LockerSize.values()).map(size -> LockerSetting.of(
            size,
            Integer.MAX_VALUE)).toArray(LockerSetting[]::new);
        return new Cabinet(settings);
    }

    static Cabinet createCabinetWithFullLockers(LockerSize[] fullLockers, int defaultCapacity) {
        LockerSetting[] setting =
            Arrays.stream(LockerSize.values()).map(size -> LockerSetting.of(size, defaultCapacity))
            .toArray(LockerSetting[]::new);

        Cabinet cabinet = new Cabinet(setting);
        for (LockerSize lockerSize : fullLockers) {
            for (int i = 0; i < defaultCapacity; i++) {
                cabinet.save(new Bag(getBagSizeFromLockerSize(lockerSize)), lockerSize);
            }
        }

        return cabinet;
    }

    private static BagSize getBagSizeFromLockerSize(LockerSize lockerSize) {
        return BagSize.valueOf(lockerSize.toString());
    }
}
